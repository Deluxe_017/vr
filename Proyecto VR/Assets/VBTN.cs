using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VBTN : MonoBehaviour
{
    public GameObject imagen;
    public GameObject boton;
    public GameObject boton2;
    public GameObject ingredientes;

    private void Update()
    {
        OnTouchCount();
    }
     private void OnTouchCount()
    {
        if (Input.touchCount > 0)
        {
            imagen.SetActive(true);
            boton.SetActive(false);
            boton2.SetActive(false);
            ingredientes.SetActive(false);

            if(imagen == true)
            {
                Invoke("Desactivar", 3f);
            }
        }
    }

    private void Desactivar()
    {
        imagen.SetActive(false);
    }
}
